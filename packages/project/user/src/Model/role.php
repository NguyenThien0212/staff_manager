<?php

namespace project\user\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class role extends Model
{
    //
    protected $table = 'role';
    public function User(){
        return $this->hasMany('project\user\Model\User','role_id', 'id');
    }
}
