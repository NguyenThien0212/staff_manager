<?php
//    route::get('admin', function (){
//        echo 'Hello word!';
//    });
Route::get('/', function (){
    return view('user::welcome');
});
Route::group(['middleware'=>['web']], function(){
Route::get('/login', 'project\user\Controllers\Auth\LoginController@ShowLoginForm')->name('login');
Route::post('/login', 'project\user\Controllers\Auth\LoginController@Login');
Route::post('/logout', 'project\user\Controllers\Auth\LoginController@logout')->name('logout');
Route::get('/register', 'project\user\Controllers\Auth\RegisterController@ShowRegister')->name('register');
Route::post('/register', 'project\user\Controllers\Auth\RegisterController@Register');

Route::get('/home', 'project\user\Controllers\UserController@index')->name('home');
Route::get('/inf_user', 'project\user\Controllers\UserController@inf_user')->name('inf_user');
//Route::post('/inf_user', 'project\user\Controllers\UserController@inf_user');
Route::get('/list_user', 'project\user\Controllers\UserController@list_user')->name('list_user');
Route::get('edit_user/{id}', 'project\user\Controllers\UserController@edit_user');
Route::post('edit_user', 'project\user\Controllers\UserController@post_edit_user')->name('post_edit_user');
Route::get('delete_user/{id}', 'project\user\Controllers\UserController@delete_user');
});
