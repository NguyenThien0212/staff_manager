@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Thông Tin Nhân Viên</div>

                    <div class="card-body">
                        <div class="box-body">
                            <form action="{{route('inf_user')}}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                                @csrf
                                <table id="example1" class="table table-bordered table-striped">
                                    <tr>
                                        <th>Nhân viên</th>
                                        <th><label> {{ Auth::user()->name }}</label></th>
                                    </tr>
                                    <tr>
                                        <th>Số Điện Thoại</th>
                                        <th><label> {{ Auth::user()->Sdt }}</label></th>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <th><label> {{ Auth::user()->email }}</label></th>
                                    </tr>
                                    <tr>
                                        <td><a href="{{ url('/list_user') }}" >Danh sách Nhân viên</a></td>
                                    </tr>
                                </table>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
