@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Danh Sách Nhân Viên</div>

                    <div class="card-body">
                        <div class="box-body">
                            @foreach($users as $user)
                            <form action=" {{ url('/edit_user',['id'=>$user->id])}}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="{{ $user->id }}">
                                @endforeach
                                @csrf
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Nhân viên</th>
                                        <th>Số Điện Thoại</th>
                                        <th>Email</th>
                                        <th>Chức Vụ</th>
                                        <th>Sửa</th>
                                    </tr>
                                    </thead>
                                    @foreach( $users as $user)
                                    <tr>

                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->Sdt }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->role->name }}</td>
                                        <td><a href="{{ url('/edit_user',['id'=>$user->id]) }}" >sửa</a></td>
                                        <td><a href="{{ url('/delete_user',['id'=>$user->id]) }}" >Xóa</a></td>
                                    </tr>
                                    @endforeach
                                </table>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
