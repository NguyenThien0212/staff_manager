@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Nhân Viên</div>

                    <div class="card-body">
                        <div class="box-body">
                            <form action="{{route('post_edit_user')}}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="{{ $users->id }}">
                                @csrf
                                <table id="example1" class="table table-bordered table-striped">
                                    <tr>
                                        <th>Nhân viên</th>
                                        <th><input type="text" name = "name" value="{{ $users->name }}"></th>
                                    </tr>
                                    <tr>
                                        <th>Số Điện Thoại</th>
                                        <th><input type="text" name = "Sdt" value="{{ $users->Sdt }}"></th>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <th><input type="text" name = "email" value="{{ $users->email }}"></th>
                                    </tr>
                                    <tr>
                                        <th>Chức Vụ</th>
                                        <th>
                                            <select name="role">
                                                @foreach($role as $roles)
                                                <option value = "{{ $roles->id }}">{{$roles->name}}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                    </tr>

                                    <tr>
                                        <td><input type="submit" value="Sửa"></td>
                                    </tr>
                                </table>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

