<?php

namespace project\user\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use project\user\Model\User;
use project\user\Model\role;
class UserController extends Controller
{
    //
//    public function add($a, $b){
//        $c = $a + $b;
//        return view('user::add',compact('c'));
//    }
//
//    public function subtract($a){
//            $a;
//     //   dd($a);
//        return view('user::add', ['a'=>$a]);
//    }
//    public function admin(){
//        return view()
//    }
    public  function  index(){
        if(Auth::user()->role_id == 1){
        return view('user::home');
        }else{
            return view('user::auth.login');
        }
    }
    public  function inf_user(){
 //       $user = User::all();
        return view('user::layout.inf_user');
    }
    public function list_user(){
        $user = User::all();
        return view('user::layout.list_user', ['users'=>$user]);
    }
    public function edit_user($id){
        $role = role::all();
        $user = User::find($id);
        return view('user::layout.edit_user', ['users'=>$user, 'role'=>$role]);
    }
    public function post_edit_user(Request $request){
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->Sdt  = $request->Sdt;
        $user->email = $request->email;
        $user->role_id = $request->role;
        $user->save();
            return redirect('/list_user');
    }
    public function delete_user($id){
        $user = User::destroy($id);
        return redirect('/list_user');
    }
}
